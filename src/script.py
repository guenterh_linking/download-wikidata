#     download-wikidata
#     Copyright (C) 2019 Project swissbib <http://swissbib.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU Affero General Public License as published
#     by the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU Affero General Public License for more details.
#
#     You should have received a copy of the GNU Affero General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
import json
import logging
import os
import shutil
import sys
from datetime import datetime

import requests
from kazoo.client import KazooClient
from kazoo.client import KazooState
from simple_elastic import ElasticIndex

logging.basicConfig(stream=sys.stdout,
                    level=logging.INFO,
                    format='[%(levelname)s]:%(module)s:%(message)s')
logging.info("Wikidata Downloader Version 1.3.1")


def create_index(path: str, config: dict, date: datetime):
    with open(path, 'r') as config_file:
        index_mapping = json.load(config_file)
    index = ElasticIndex(f'{config["name"]}-{date.strftime("%Y-%m-%d")}',
                         url=elastic_search_host,
                         mapping=index_mapping,
                         settings=index_settings)
    if 'alias' in config:
        index.add_to_alias(config['alias'])


def my_listener(state: KazooState):
    if state == KazooState.LOST:
        logging.warning("Connection to zookeeper lost. Attempt to reconnect.")
    elif state == KazooState.SUSPENDED:
        logging.info("Connection to zookeeper suspended. Attempt to reconnect!")
    else:
        logging.info("Reconnected to zookeeper.")


zk = None
try:
    tmp_path = '/tmp/latest-truthy-dump.nt.bz2'

    if os.path.exists('/configs/config.json'):
        with open('/configs/config.json') as file:
            configs = json.load(file)
    else:
        logging.warning("Loading default configuration from container!")
        with open('config.json') as file:
            configs = json.load(file)

    with open(f'/configs/settings.json', 'r') as fp:
        index_settings = json.load(fp)

    elastic_search_host = configs['host']

    zk = KazooClient(hosts=configs['zoo']['hosts'])
    zk.start()
    zk.add_listener(my_listener)

    zookeeper_base_node = configs['zoo']['node']
    zookeeper_last_modified_node = zookeeper_base_node + '/last_modified'
    zk.ensure_path(zookeeper_last_modified_node)

    try:
        logging.info(f"Download latest dump from {configs['url']}.")
        with requests.get(configs['url'], stream=True) as response:
            if response.ok:
                last_modified = response.headers.get('Last-Modified')
                logging.info(f"Dump Last-Modified {last_modified}")
                latest = datetime.strptime(last_modified, configs["datePattern"])

                previous_last_modified, meta = zk.get(zookeeper_last_modified_node)
                previous_last_modified = previous_last_modified.decode('utf-8')

                if previous_last_modified != '':
                    logging.info(f"Last downloaded dump {previous_last_modified}.")
                    previous = datetime.strptime(previous_last_modified, configs["datePattern"])
                else:
                    logging.info("No previous dump recorded. Downloading dump!")
                    previous = None

                if previous is None or latest > previous:
                    logging.info("Prepare indices for latest dump")
                    create_index('/configs/persons.json', configs['persons'], latest)
                    create_index('/configs/organisations.json', configs['organisations'], latest)
                    create_index('/configs/events.json', configs['events'], latest)
                    create_index('/configs/items.json', configs['items'], latest)
                    logging.info("Created indices for indexing latest dump.")
                    content_length = int(response.headers.get('Content-Length'))
                    logging.info(f"Downloading new dump with a total of {content_length / 1000000000} GB.")
                    current_download = 0
                    with open('/tmp/latest-truthy-dump.nt.bz2', 'wb') as f:
                        for chunk in response.iter_content(chunk_size=1200000):
                            f.write(chunk)
                            current_download += len(chunk)
                            logging.info(
                                f"Downloaded {current_download / 1000000000} GB / {content_length / 1000000000} GB")
                        f.flush()

                    file_size = os.stat('/tmp/latest-truthy-dump.nt.bz2')[6]
                    if file_size != content_length:
                        raise EOFError

                    zk.set(zookeeper_last_modified_node, last_modified.encode('utf-8'))
                    target_path = f'{configs["path"]}/wikidata-truthy-{latest.strftime("%Y-%m-%d")}.nt.bz2'
                    shutil.move('/tmp/latest-truthy-dump.nt.bz2', target_path)
                    logging.info(f"Finished downloading latest dump and stored in {target_path}")
    except requests.exceptions.ConnectionError as ex:
        logging.error("Connection error. Try again next day!")
    except EOFError as ex:
        logging.error("Could not download entire file. Restart app!")

except Exception as ex:
    logging.error(ex)
finally:
    if zk is not None:
        zk.stop()
